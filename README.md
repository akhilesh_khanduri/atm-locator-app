# Steps to run the service locally

* run the gradle command gradle clean build
* run the class AtmLocatorApplication as spring boot application
* open link http://localhost:8080/swagger-ui.html on your browser
* access the api /atms or use curl -X GET "http://localhost:8080/atms"
  to get the list of all atms.
* access the api /atms/{city} or use curl -X GET "http://localhost:8080/atms/city_name"
  to get the list of all atms for a given city.


# Steps to run the test cases locally
* run AtmLocatorApplicationTests class to check the unit tests
