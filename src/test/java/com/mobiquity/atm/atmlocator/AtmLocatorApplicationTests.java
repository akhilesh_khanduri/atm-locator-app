package com.mobiquity.atm.atmlocator;

import com.mobiquity.atm.atmlocator.api.AtmLocatorController;
import com.mobiquity.atm.atmlocator.domain.Address;
import com.mobiquity.atm.atmlocator.domain.Atm;
import com.mobiquity.atm.atmlocator.service.AtmLocatorService;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;

import java.util.Arrays;
import java.util.List;

import static org.hamcrest.collection.IsCollectionWithSize.hasSize;
import static org.hamcrest.core.Is.is;
import static org.mockito.BDDMockito.given;
import static org.springframework.http.MediaType.APPLICATION_JSON;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@RunWith(SpringRunner.class)
@WebMvcTest(value = AtmLocatorController.class)
public class AtmLocatorApplicationTests {

    private String URL = "http://localhost:8080//atms";

    @Autowired
    MockMvc mockMvc;

    @MockBean
    AtmLocatorService atmLocatorService;

    @Autowired
    AtmLocatorController atmLocatorController;


    @Test
    public void getListOfAtm() throws Exception {
        List<Atm> atmList = getAtmObjectList();

        given(atmLocatorController.getListOfAtms()).willReturn(atmList);

        mockMvc.perform(get(URL)
                .contentType(APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$", hasSize(2)))
                .andDo(print())
                .andExpect(jsonPath("$[0].address.city", is(atmList.get(0).getAddress().getCity())))
                .andExpect(jsonPath("$[0].address.city", is("london")))
                .andExpect(jsonPath("$[1].address.city", is(atmList.get(1).getAddress().getCity())))
                .andExpect(jsonPath("$[1].address.city", is("pune")));
    }


    @Test
    public void getAtmByCity() throws Exception {
        List<Atm> atmList = getAtmObjectList();

        given(atmLocatorController.getListOfAtmsByCity(atmList.get(1).getAddress().getCity())).willReturn(atmList);

        mockMvc.perform(get(URL + "/pune")
                .contentType(APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$[0].address.city", is(atmList.get(0).getAddress().getCity())));
    }

    private List<Atm> getAtmObjectList() {
        return Arrays.asList(createAtmObject("london"), createAtmObject("pune"));
    }

    private Atm createAtmObject(String city) {
        Address address = new Address();
        address.setCity(city);
        Atm atm = new Atm();
        atm.setAddress(address);
        return atm;
    }

}
