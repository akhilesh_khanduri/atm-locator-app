package com.mobiquity.atm.atmlocator.exceptions;

public class AtmNotFoundException extends Exception {
    public AtmNotFoundException() {
        super();
    }

    public AtmNotFoundException(String message) {
        super(message);
    }

    public AtmNotFoundException(String message, Throwable cause) {
        super(message, cause);
    }

    public AtmNotFoundException(Throwable cause) {
        super(cause);
    }

    protected AtmNotFoundException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
        super(message, cause, enableSuppression, writableStackTrace);
    }
}
