package com.mobiquity.atm.atmlocator.exceptions.handlers;


import com.mobiquity.atm.atmlocator.domain.ErrorMessage;
import com.mobiquity.atm.atmlocator.exceptions.AtmNotFoundException;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.context.request.WebRequest;

@ControllerAdvice
@ResponseStatus
public class AtmExceptionHandler {

    @ExceptionHandler(AtmNotFoundException.class)
    public ResponseEntity<ErrorMessage> atmNotFoundExceptionHandler(AtmNotFoundException exception,
                                                                    WebRequest request) {
        return ResponseEntity
                .status(HttpStatus.NOT_FOUND)
                .body(new ErrorMessage(HttpStatus.NOT_FOUND, exception.getMessage()));
    }

}
