package com.mobiquity.atm.atmlocator.api;

import com.mobiquity.atm.atmlocator.domain.Atm;
import com.mobiquity.atm.atmlocator.exceptions.AtmNotFoundException;
import com.mobiquity.atm.atmlocator.service.AtmLocatorService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping("/")
public class AtmLocatorController {

    private static final Logger LOGGER = LoggerFactory.getLogger(AtmLocatorController.class);

    @Autowired
    AtmLocatorService atmLocatorService;

    @GetMapping("/atms")
    public List<Atm> getListOfAtms() {
        List<Atm> allAtms = atmLocatorService.getAllAtms();
        LOGGER.info("response : " + allAtms);
        return allAtms;
    }

    @GetMapping("/atms/{city}")
    public List<Atm> getListOfAtmsByCity(@PathVariable String city) throws AtmNotFoundException {
        List<Atm> allAtmsByCityName = atmLocatorService.getAtmsByCity(city);
        LOGGER.info("response : " + allAtmsByCityName);
        return allAtmsByCityName;
    }
}
