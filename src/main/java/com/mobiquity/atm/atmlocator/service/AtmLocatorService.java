package com.mobiquity.atm.atmlocator.service;


import com.mobiquity.atm.atmlocator.api.AtmLocatorController;
import com.mobiquity.atm.atmlocator.domain.Atm;
import com.mobiquity.atm.atmlocator.exceptions.AtmNotFoundException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;
import org.springframework.web.client.RestTemplate;

import java.util.List;
import java.util.stream.Collectors;

@Component
public class AtmLocatorService {

    private static final Logger LOGGER = LoggerFactory.getLogger(AtmLocatorController.class);

    @Autowired
    RestTemplate restTemplate;

    @Value("${atm.service.endpoint}")
    private String URL;


    public List<Atm> getAllAtms() {
        LOGGER.info("fetching list of atms using URL : {}", URL);
        ResponseEntity<List<Atm>> responseEntity = restTemplate.exchange(URL, HttpMethod.GET, null, new ParameterizedTypeReference<List<Atm>>() {
        });
        return responseEntity.getBody();
    }

    public List<Atm> getAtmsByCity(String city) throws AtmNotFoundException {
        LOGGER.info("fetching list of atms for the CITY : {}", city);
        List<Atm> atmsByCity = getAllAtms()
                .stream()
                .collect(Collectors.partitioningBy(atm -> city.equalsIgnoreCase(atm.getAddress().getCity())))
                .get(true);

        if (atmsByCity.isEmpty())
            throw new AtmNotFoundException("ATM not found for the city : " + city);

        return atmsByCity;
    }
}
