package com.mobiquity.atm.atmlocator.domain;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class Hours {
    private String hourFrom;
    private String hourTo;
}
