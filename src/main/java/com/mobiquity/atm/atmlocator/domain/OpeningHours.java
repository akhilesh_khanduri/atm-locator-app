package com.mobiquity.atm.atmlocator.domain;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;


@Data
@NoArgsConstructor
@AllArgsConstructor
public class OpeningHours {
    private int dayOfWeek;
    private Hours[] hours;
}
