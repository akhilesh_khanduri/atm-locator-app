package com.mobiquity.atm.atmlocator.domain;


import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class Address {
    private String street;
    private String housenumber;
    private String postalcode;
    private String city;
    private GeoLocation geoLocation;
}
