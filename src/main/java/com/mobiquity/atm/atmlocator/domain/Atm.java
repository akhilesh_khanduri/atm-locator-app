package com.mobiquity.atm.atmlocator.domain;


import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class Atm {
    private Address address;
    private double distance;
    private OpeningHours[] openingHours;
    private String functionality;
    private String type;
}
